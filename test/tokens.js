import { expect } from 'chai'
import { schema } from '@uniswap/token-lists'
import Ajv from 'ajv'
import addFormats from 'ajv-formats'
import fetch from 'node-fetch'
import * as fs from 'fs';

const tokenfolder = 'tokens'
const validChains = [1, 941, 369]

fs.readdirSync(tokenfolder).forEach(file => {

	describe(file, () => {
		let token;

		it('parse', () => {
			token = JSON.parse(fs.readFileSync(tokenfolder + '/' + file))
		}) 

		it('chain', () => {
			let tokenChains = (Array.isArray(token.chainId) ? token.chainId : [token.chainId])
			expect( validChains ).to.include.members(tokenChains)
			token.chainId = tokenChains[0]
		}) 

		it('icon', async () => {
			let resp = await fetch(token.logoURI)
			expect(resp.ok).to.eq(true)
		}) 

		it('valid', () => {
			let list = {
				name:      "Test",
				keywords:  ["test"],
				timestamp: new Date().toISOString(),
				tags:     {
				  "stablecoin": {"name": "test","description": "test"},
				  "meme":       {"name": "test","description": "test"},
				  "utility":    {"name": "test","description": "test"},
				  "governance": {"name": "test","description": "test"},
				  "security":   {"name": "test","description": "test"},
				  "yield":      {"name": "test","description": "test"},
				  "priority":   {"name": "test","description": "test"}
				},
				tokens:    [token],
				version:   {major: 1, minor: 0, patch: 0}
			}

			const ajv = new Ajv({ allErrors: false, verbose: false })
			addFormats(ajv)
			const validator = ajv.compile(schema);
			const valid     = validator(list)

			if(!valid) {
				let error = validator.errors[0]
				throw(error.instancePath + ' ' + error.message)
			}

			expect(valid).to.eq(true)
		})
	})
})
