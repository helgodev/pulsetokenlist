PulseChain Token List
=====================
Community managed token list for PulseChain based on https://tokenlists.org/

Used as the primary source of https://PulseTokens.org


Clone the project
-----------------

```
git clone https://gitlab.com/pulsetokens/pulsetokenlist.git
cd pulsetokenlist
npm install
```

Adding a token
--------------
Only verified tokens can be added to this list.

1. Create a new json file based on template.json matching the token symbol in tokens ie. token.json
2. Run "npm run test" and ensure there are no errors
3. Create a pull request on gitlab for new file

Token verification
------------------
See the below articles for help verifying with the PulseChain block explorer.

https://www.hexpulse.info/docs/hardhat-development

https://www.hexpulse.info/docs/truffle-development

